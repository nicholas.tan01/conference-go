from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def find_pictures(search_term):
    url = "https://api.pexels.com/v1/search?query=" + search_term

    print(PEXELS_API_KEY)

    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)

    data = json.loads(response.content)

    photos = data["photos"]
    if photos and len(photos) > 0:
        return photos


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",US&limit=limit5&appid="
        + OPEN_WEATHER_API_KEY
    )
    response = requests.get(url)
    data = json.loads(response.content)[0]
    lon = data["lon"]
    lat = data["lat"]

    print(lon, lat)
    return {}
