import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
import time
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    if body is not None:
        data = json.loads(body)
        send_mail(
            "Your presentation has been accepted",
            f'{data.get("presenter_name")}, we are happy to tell you that your presentation {data.get("title")} has been accepted',
            "admin@conference.go",
            [data.get("presenter_email")],
            fail_silently=False,
        )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="process_approvals")
        channel.queue_declare(queue="process_rejections")
        channel.basic_consume(
            queue="process_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        # channel.basic_consume(
        #     queue="process_rejections",
        #     on_message_callback=process_rejection,
        #     auto_ack=True,
        # )
        print(" [*] Waiting for messages. To exit press CTRL+C")
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
